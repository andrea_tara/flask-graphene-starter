from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
import graphene
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from flask_graphql import GraphQLView
#################################
app = Flask(__name__)
app.debug = True

basedir = os.path.abspath(os.path.dirname(__file__))
# Configs
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' +    os.path.join(basedir, 'data.sqlite')
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
# Modules
db = SQLAlchemy(app)

# Models
class User(db.Model):
	__tablename__ = 'users'
	uuid = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(256), index=True, unique=True)
	posts = db.relationship('Post', backref='author')

	def __repr__(self):
		return '<User %r>' % self.username

class Post(db.Model):
	__tablename__ = 'posts'
	uuid = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(256), index=True)
	body = db.Column(db.Text)
	author_id = db.Column(db.Integer, db.ForeignKey('users.uuid'))
	def __repr__(self):
		return '<Post %r>' % self.title


# Schema Objects
class PostObject(SQLAlchemyObjectType):
	class Meta:
		model = Post
		filter_fields = ['title','username', 'uuid']
		interfaces = (graphene.relay.Node, )
class UserObject(SQLAlchemyObjectType):
   class Meta:
	   model = User
	   filter_fields = ['username', 'uuid']
	   interfaces = (graphene.relay.Node, )

# Connection
class PostConnection(SQLAlchemyConnectionField):
	@classmethod
	def get_query(cls, model, info, **args):
		print(args)
		print
		if 'title' in args:
			return model.query.filter_by(username=args['title'])
		if 'id' in args:
			return model.query.filter_by(uuid=args['id'])
		return model.query


class Query(graphene.ObjectType):
	node = graphene.relay.Node.Field()
	postCursor = PostConnection(
		PostObject, sort=None, 
		args={'title': graphene.Argument(graphene.String)
		,'id':graphene.Argument(graphene.Int)})

	posts = graphene.List(PostObject,id=graphene.Int(),
		title=graphene.String(),author=graphene.String())

	users = graphene.List(UserObject,id=graphene.Int(),
		username=graphene.String())

	filter_user_posts = graphene.List(
		UserObject,
		username=graphene.String())

	def resolve_filter_user_posts(self, info, username):
		query = UserObject.get_query(info)
		query = query.join(User.posts)
		query = query.filter(User.username == username)
		objs = query.all()

		return objs

	def resolve_posts(self, info, title=None, id=None, author=None):
		query = PostObject.get_query(info)
		if title:
			return query.filter(Post.title==title)
		if id:
			return query.filter(User.uuid==id)
		if author:
			query = query.join(User.posts)
			query = query.filter(User.username == author)
		return query.all()
	
	def resolve_users(self, info, **args):
		#print("resolve users: "+str(args))
		#print
		query = UserObject.get_query(info)
		if 'username' in args:
			return query.filter(User.username==args['username'])
		if 'id' in args:
			return query.filter(User.uuid==args['id'])
		return query.all()


class CreatePost(graphene.Mutation):
	class Arguments:
		title = graphene.String(required=True)
		body = graphene.String(required=True)
		author = graphene.String(required=True)
	post = graphene.Field(lambda: PostObject)
	def mutate(self, info, title, body, author):
		user = User.query.filter_by(username=author).first()
		post = Post(title=title, body=body)
		if user is not None:
			post.author = user
		db.session.add(post)
		db.session.commit()
		return CreatePost(post=post)

class CreateUser(graphene.Mutation):
	class Arguments:
		username = graphene.String(required=True)
	user = graphene.Field(lambda: UserObject)
	def mutate(self, info, username):
		user = User(username=username)
		db.session.add(user)
		db.session.commit()
		return CreateUser(user=user)

class Mutation(graphene.ObjectType):
	create_post = CreatePost.Field()
	create_user = CreateUser.Field()

schema = graphene.Schema(query=Query, mutation=Mutation, types=[UserObject, PostObject])
#### views
@app.route('/')
def index():
	return '<p> Hello World!</p>'

app.add_url_rule(
	'/graphql',
	view_func=GraphQLView.as_view(
		'graphql',
		schema=schema,
		graphiql=True # for having the GraphiQL interface
	)
)

##################################
if __name__ == '__main__':
	 app.run(host='0.0.0.0')